[![pipeline status](https://gitlab.com/skew202/ssg/badges/master/pipeline.svg)](https://gitlab.com/skew202/ssg/commits/master)

# static site generator

[See the demo here](https://skew202.gitlab.io/ssg/)

## Get started

1. Run `npm i && npm run start` to get everything up and running.
2. Create pages in Markdown | HTML | EJS inside the `pages` directory.
3. Create JSON inside the `data` directory, require it in `config` and use it in ejs.
4. Add stylesheets, scripts and images inside the `scss`, `js` and `images` directories.
5. Check the changes on `localhost:8080`.
6. Grab everything inside the `public` directory and publish with [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).


## Credits

- [Fully Responsive HTML5 + CSS3, Customizable, 100% Free theme](https://html5up.net/)
