const socials = require('./src/data/socials')

/*
  Depending on the target domain.
  Currently the repo name in gitlab
  e.g. "ssg.git"
 */
const basePath = process.env.CI ? '../' : '/';

module.exports = {
  site: {
    basePath,
    title: 'ssg',
    description: 'Static Site Generator',
    socials
  }
}
