const fse = require('fs-extra')
const path = require('path')
const ejs = require('ejs')
const util = require('util')
const marked = require('marked')
const frontMatter = require('front-matter')
require('util.promisify').shim();
const ejsRenderFile = util.promisify(require('ejs').renderFile)
const globP = util.promisify(require('glob'))
const config = require('./config')

// All paths are relative to package.json.
const srcPath = './src'
const distPath = './public' // Gitlab pages require this foldername
const assetPaths = ['css', 'fonts', 'images', 'js']

// Clear destination folder
fse.emptyDirSync(distPath)

// Copy folders with assets into the output folder.
console.log('Copying folders...')
try {
  for(var folder of assetPaths){
    fse.copySync(path.join(`./src/assets/${folder}`), path.join(`${distPath}/assets`,folder))
  }
}
catch (err){
  console.log('Error during folder copying: ' + err);
  process.exit(1)
}

// Read page templates
console.log('Generating pages...')
globP('**/*.@(md|ejs|html)', { cwd: `${srcPath}/pages` })
  .then((files) => {
    files.forEach((file) => {
      const fileData = path.parse(file)
      const destPath = path.join(distPath, fileData.dir)

      // Create destination directory
      fse.mkdirs(destPath)
       .then(() => {
          // Read page file
          return fse.readFile(`${srcPath}/pages/${file}`, 'utf-8')
        })
        .then((data) => {
          // Render page
          const pageData = frontMatter(data)
          const templateConfig = Object.assign({}, config, { page: pageData.attributes })
          let pageContent

          // Generate page content according to file type
          switch (fileData.ext) {
            case '.md':
              pageContent = marked(pageData.body)
              break
            case '.ejs':
              pageContent = ejs.render(pageData.body, templateConfig)
              break
            default:
              pageContent = pageData.body
          }

          // Render layout with page contents
          const layout = pageData.attributes.layout || 'default'

          return ejsRenderFile(`${srcPath}/layouts/${layout}.ejs`, Object.assign({}, templateConfig, { body: pageContent }))
        })
        .then((str) => {
          // Save the html file
          fse.writeFile(`${destPath}/${fileData.name}.html`, str)
          console.log('Compiled', file)
        })
        .catch((err) => { console.error(err) })
    })
  })
  .catch((err) => { console.error(err) })
